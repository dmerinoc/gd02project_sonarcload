package es.unex.giiis.asee.zuni;

import org.junit.Test;

import es.unex.giiis.asee.zuni.api.hourlies.HourMinimal;

import static org.junit.Assert.assertEquals;


public class CU06 {

    private static final double DELTA = 1e-15;

    @Test
    public void shouldGetterSetter(){
        //Creamos una instancia de HourMinimal
        HourMinimal hourM = new HourMinimal();

        //Definimos variables con los datos a insertar
        long id = 123;
        String main = "Prueba";
        Long dt = 789456123000L;
        String description= "descripcionPrueba";
        double pop = 23.3;
        Double windSpeed = 24.4;

        //Insertamos los valores usando Setters
        hourM.setId(id);
        hourM.setMain(main);
        hourM.setDt(dt);
        hourM.setDescription(description);
        hourM.setPop(pop);
        hourM.setWindSpeed(windSpeed);

        // Des. junior
        hourM.setTemp(37.5);
        hourM.setFeelsLike(42.1);
        hourM.setCity("Sevilla");
        hourM.setCountryCode("ES");

        //Se prueban los getters
        assertEquals(id, hourM.getId());
        assertEquals(main, hourM.getMain());
        assertEquals(dt, hourM.getDt());
        assertEquals(description,hourM.getDescription());
        assertEquals(pop,hourM.getPop(),DELTA);
        assertEquals(windSpeed,hourM.getWindSpeed());

        assertEquals(hourM.getTemp(),37.5, DELTA);
        assertEquals(hourM.getFeelsLike(),42.1,DELTA);
        assertEquals(hourM.getCity(),"Sevilla");
        assertEquals(hourM.getCountryCode(),"ES");

    }
}
