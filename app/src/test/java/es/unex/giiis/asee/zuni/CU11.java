package es.unex.giiis.asee.zuni;

import org.junit.Test;

import java.util.Date;

import es.unex.giiis.asee.zuni.eventos.Evento;

import static org.junit.Assert.assertEquals;

public class CU11 {

    @Test
    public void shouldgetDAOeventos(){

        Date fecha = new Date(120,5,3); //03/05/2020

        //Primera parte, crear Evento con datos

        Double lat = 40.9;
        Double lon = 40.1;
        Evento evento = new Evento(999, "Titulo", "Desc", fecha, Evento.Alerta.ALTA, "Madrid", lat, lon);

        assertEquals(evento.getId(),999);
        assertEquals(evento.getTitulo(),"Titulo");
        assertEquals(evento.getDescripcion(),"Desc");
        assertEquals(evento.getFecha(),fecha);
        assertEquals(evento.getAlerta(),Evento.Alerta.ALTA);
        assertEquals(evento.getUbicacion(),"Madrid");
        assertEquals(evento.getLat(),lat);
        assertEquals(evento.getLon(),lon);
    }
}
