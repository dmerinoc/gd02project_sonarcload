package es.unex.giiis.asee.zuni;

import android.view.Gravity;

//import androidx.test.espresso.contrib.DrawerActions;
//import androidx.test.espresso.contrib.DrawerMatchers;
//import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.DrawerMatchers;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
//import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CU11 {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testing() throws InterruptedException {
        String testString = "Rescatar a Luke en Hoth";
        String testString1 = "Llevar Tauntaun";

        // Abrir drawer
        onView(withId(R.id.drawer_layout)).check(matches(DrawerMatchers.isClosed(Gravity.LEFT)))
                .perform(DrawerActions.open());

        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_eventos));

        Thread.sleep(1000);

        // obtenemos el numero de elementos
        RecyclerViewItemCountAssertion r = new RecyclerViewItemCountAssertion(0);
        onView(withId(R.id.eventos_recycler_view)).check(r);
        int num = r.count;



        onView(withId(R.id.addEventoFab)).perform(click());

        Thread.sleep(1000);

        onView(withId(R.id.eventoTituloInput)).perform(typeText(testString), closeSoftKeyboard());



        onView(withId(R.id.eventoDescripcionInput)).perform(typeText(testString1), closeSoftKeyboard());



        onView(withId(R.id.submitButton)).perform(click());

        Thread.sleep(1000);

        r.setExpected(num+1);
        onView(withId(R.id.eventos_recycler_view)).check(r);

        onView(withId(R.id.eventos_recycler_view)).perform(
                RecyclerViewActions.actionOnItemAtPosition(num,click()));


        Thread.sleep(1000);
        onView(withId(R.id.tituloEventoTV)).check(matches(withText(testString)));


        onView(withId(R.id.descripcionEventoTV)).check(matches(withText(testString1)));
    }
}
