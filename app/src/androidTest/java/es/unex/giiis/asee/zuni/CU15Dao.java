package es.unex.giiis.asee.zuni;
import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import es.unex.giiis.asee.zuni.ubicaciones.Ubicacion;
import es.unex.giiis.asee.zuni.ubicaciones.db.UbicacionDao;
import es.unex.giiis.asee.zuni.ubicaciones.db.UbicacionDatabase;

@RunWith(AndroidJUnit4.class)
public class CU15Dao {
    private UbicacionDao ubicacionDao;
    private UbicacionDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db =Room.inMemoryDatabaseBuilder(context, UbicacionDatabase.class).build();
        ubicacionDao = db.getDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void shouldDeleteLocation(){
        /* Desarrollador Senior */

        // Crear una ubicacion
        Ubicacion ubicacion = new Ubicacion(999, "Bilbao", 40.5, 50.5, "ES");
        // Se inserta la ubicacion en la base de datos
        ubicacionDao.insert(ubicacion);


        /* Desarrollador Junior */


        //Se comprueba que se recupera la ubicacion de la BD
        assertEquals(ubicacion.getId(), ubicacionDao.getUbicacion(ubicacion.getId()).getId());

        //Se borra la ubicacion de la base de datos
        ubicacionDao.delete(ubicacion.getId());

        assertNull(ubicacionDao.getUbicacion(ubicacion.getId()));
    }
}
