package es.unex.giiis.asee.zuni;
import android.view.Gravity;

import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.DrawerMatchers;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

/* ============================================================================================== */
/* CU10 : Busqueda de historico por nombre y country code y guardar ============================= */
/* ============================================================================================== */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CU10 {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void shouldAddHistoricalByCityName() throws InterruptedException {

        /* EL HISTORICO DEBE ESTAR VACIO */

        String testString = "Barcelona";

        /* NAVEGACION --------------------------------------------------------------------------- */

        // Abrir drawer
        onView(withId(R.id.drawer_layout)).check(matches(DrawerMatchers.isClosed(Gravity.LEFT)))
                .perform(DrawerActions.open());
        // Navegar a historico
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_historical));
        // Pausa para cargar la vista
        Thread.sleep(500);

        /* PANTALLA HISTORICO --------------------------------------------------------------------- */

        // Hacemos la busqueda por ciudad
        onView(withId(R.id.et_city)).perform(typeText(testString));
        // Consultamos los historicos guardados para la ciudad dada
        onView(withId(R.id.button1)).perform(click(), closeSoftKeyboard());

        Thread.sleep(4000);

        // obtenemos el numero de elementos
        RecyclerViewItemCountAssertion r = new RecyclerViewItemCountAssertion(0);
        onView(withId(R.id.listHistorical)).check(r);
        int num = r.count;

        // Click añadir historico
        onView(withId(R.id.addHistorical)).perform(click());

        /* PANTALLA GUARDAR HISTORICO ----------------------------------------------------------- */

        // Hacemos la busqueda por ciudad
        onView(withId(R.id.et_city)).perform(typeText(testString));
        // Consultamos los historicos guardados para la ciudad dada
        onView(withId(R.id.buttonSearch1)).perform(click(), closeSoftKeyboard());

        Thread.sleep(4000);

        // Click en guardar evento
        onView(withId(R.id.buttonSave1)).perform(click());



        /* PANTALLA HISTORICO --------------------------------------------------------------------- */

        // Consultamos los historicos guardados para la ciudad dada. Ya esta guardado el nombre de la ciudad
        onView(withId(R.id.button1)).perform(click(), closeSoftKeyboard());

        // Comprobamos si la lista tiene un elemento mas
        Thread.sleep(4000);
        r.setExpected(num+1);
        onView(withId(R.id.listHistorical)).check(r);

        // Borramos el historico
        onView(withId(R.id.bt_erase)).perform(click());
        Thread.sleep(1000);
    }
}
