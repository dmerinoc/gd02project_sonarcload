package es.unex.giiis.asee.zuni;
import android.view.Gravity;

import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.DrawerMatchers;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

/* ============================================================================================== */
/* CU02 : ESTABLECER UBICACION PREDETERMINADA =================================================== */
/* ============================================================================================== */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CU02 {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void shouldSetDefaultLocation() throws InterruptedException {
        /* LA LISTA DE UBICACIONES DEBE ESTAR VACIA */

        String testString = "Madrid";

        /* NAVEGACION --------------------------------------------------------------------------- */

        // Abrir drawer
        onView(withId(R.id.drawer_layout)).check(matches(DrawerMatchers.isClosed(Gravity.LEFT)))
                .perform(DrawerActions.open());
        // Navegar a eventos
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_ubicaciones));
        // Pausa para cargar la vista
        Thread.sleep(500);

        /* PANTALLA UBICACIONES ----------------------------------------------------------------- */

        // Click en el boton de añadir ubicacion
        onView(withId(R.id.addUbicaciones)).perform(click());

        /* PANTALLA AÑADIR UBICACION ------------------------------------------------------------ */

        // Escribir nombre de la ubicacion
        onView(withId(R.id.ubicacionUbicacionInput)).perform(typeText(testString), closeSoftKeyboard());
        // Click en buscar
        onView(withId(R.id.ubicacionBuscarButton)).perform(click(), closeSoftKeyboard());
        Thread.sleep(4000);
        // Click en guardar
        onView(withId(R.id.ubicacionSubmit)).perform(click());
        Thread.sleep(1000);

        /* PANTALLA UBICACIONES ----------------------------------------------------------------- */

        // Se guarda la ubicacion como predeterminada
        onView(withId(R.id.favButtonUbicacion)).perform(click());
        Thread.sleep(1000);
        // Confirmar
        onView(withId(R.id.yes_favUbicacion)).perform(click());
        Thread.sleep(2000);
        // Se comprueba que se ha establecido la ubicacion como predeterminada
        onView(withId(R.id.ubicaciones_recycler_view)).check(matches(hasDescendant(withText(testString + " (Ubicación predeterminada)"))));
        // Se borra la ubicacion
        onView(withId(R.id.deleteButtonUbicacion)).perform(click());
        Thread.sleep(1000);
    }
}
