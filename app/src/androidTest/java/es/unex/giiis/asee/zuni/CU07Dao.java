package es.unex.giiis.asee.zuni;


import android.content.Context;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.io.IOException;
import java.util.List;


import es.unex.giiis.asee.zuni.ubicaciones.Ubicacion;
import es.unex.giiis.asee.zuni.ubicaciones.db.UbicacionDao;
import es.unex.giiis.asee.zuni.ubicaciones.db.UbicacionDatabase;


@RunWith(AndroidJUnit4.class)
public class CU07Dao {

        private UbicacionDao ubicacionDao;
        private UbicacionDatabase db;

        @Before
        public void createDb() {
            Context context = ApplicationProvider.getApplicationContext();
            db =Room.inMemoryDatabaseBuilder(context, UbicacionDatabase.class).build();
            ubicacionDao = db.getDao();
        }

        @After
        public void closeDb() throws IOException {
            db.close();
        }

        @Test
        public void shouldUbicacionesDAO() {
            Ubicacion u1 = ubicacionDao.getUbicacion(123);
            assertEquals(null,u1);

            int beforeInsert = ubicacionDao.getAllUbicaciones().size();

            Ubicacion ubicacion = new Ubicacion(123, "test", 0.0, 0.0, false, "nc");
            ubicacionDao.insert(ubicacion);

            Ubicacion u2 = ubicacionDao.getUbicacion(123);
            assertEquals(u2.getId(),123);

            int afterInsert = ubicacionDao.getAllUbicaciones().size();
            assertEquals(beforeInsert+1,afterInsert);

            //TODO  Completar


    }

}