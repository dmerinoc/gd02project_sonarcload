package es.unex.giiis.asee.zuni;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import es.unex.giiis.asee.zuni.eventos.Evento;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import es.unex.giiis.asee.zuni.eventos.db.EventoDao;
import es.unex.giiis.asee.zuni.eventos.db.EventoDatabase;

@RunWith(AndroidJUnit4.class)
public class CU11Dao {

    private EventoDao eventoDao;
    private EventoDatabase db;

    @Before
    public void createDb() {
        Context context = ApplicationProvider.getApplicationContext();
        db =Room.inMemoryDatabaseBuilder(context, EventoDatabase.class).build();
        eventoDao = db.getDao();
    }

    @After
    public void closeDb() throws IOException {
        db.close();
    }

    @Test
    public void shouldDetallesDao() throws Exception {
        Date fecha = new Date(120,5,3); //03/05/2020

        //Primera parte, crear Evento con datos

        Double lat = 40.9;
        Double lon = 40.1;
        Evento evento = new Evento(999, "Titulo", "Desc", fecha, Evento.Alerta.ALTA, "Madrid", lat, lon);


        if (eventoDao.getEvento(999) == null) {

            int beforeInsert = eventoDao.getAllEventos().size();
            eventoDao.insert(evento);
            int afterInsert = eventoDao.getAllEventos().size();
            Evento eRecuperado = eventoDao.getEvento(999);
            assertEquals(beforeInsert+1,afterInsert);

            // Desarrollador Junior
            // Comprobar que el evento recuperado de la BD tiene los mismos atributos que el insertado anteriormente
            assertEquals(eRecuperado.getId(), evento.getId());
            assertEquals(eRecuperado.getTitulo(), evento.getTitulo());
            assertEquals(eRecuperado.getDescripcion(), evento.getDescripcion());
            assertEquals(eRecuperado.getFecha(), evento.getFecha());
            assertEquals(eRecuperado.getAlerta(), evento.getAlerta());
            assertEquals(eRecuperado.getUbicacion(), evento.getUbicacion());
            assertEquals(eRecuperado.getLat(), evento.getLat());
            assertEquals(eRecuperado.getLon(), evento.getLon());
        }
    }


}
