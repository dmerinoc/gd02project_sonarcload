package es.unex.giiis.asee.zuni;
import android.view.Gravity;

import androidx.test.espresso.contrib.DrawerActions;
import androidx.test.espresso.contrib.DrawerMatchers;
import androidx.test.espresso.contrib.NavigationViewActions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

/* ============================================================================================== */
/* CU16 : EDITAR UN EVENTO ====================================================================== */
/* ============================================================================================== */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class CU16 {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void shouldEditEvent() throws InterruptedException {
        String eventoInsertadoStr = "EVENTO";
        String eventoModificadoStr = " MODIFICADO";

        /* NAVEGACION --------------------------------------------------------------------------- */

        // Abrir drawer
        onView(withId(R.id.drawer_layout)).check(matches(DrawerMatchers.isClosed(Gravity.LEFT)))
                .perform(DrawerActions.open(), closeSoftKeyboard());
        // Navegar a previsiones por dia
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.nav_eventos), closeSoftKeyboard());
        // Pausa para cargar la vista
        Thread.sleep(500);

        /* PANTALLA EVENTOS --------------------------------------------------------------------- */

        // Click añadir evento
        onView(withId(R.id.addEventoFab)).perform(click());

        /* PANTALLA AÑADIR EVENTO --------------------------------------------------------------- */

        // Escribir titulo
        onView(withId(R.id.eventoTituloInput)).perform(typeText(eventoInsertadoStr), closeSoftKeyboard());
        // Escribir descripcion
        onView(withId(R.id.eventoDescripcionInput)).perform(typeText(eventoInsertadoStr), closeSoftKeyboard());
        // Click en Submit
        onView(withId(R.id.submitButton)).perform(click());
        // Click en el primer elemento de la lista
        onView(withId(R.id.eventos_recycler_view)).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, click()));

        /* PANTALLA EVENTOS --------------------------------------------------------------------- */

        // Click en el boton de editar evento
        onView(withId(R.id.editEventoFab)).perform(click());
        Thread.sleep(500);
        // Escribir titulo
        onView(withId(R.id.eventoTituloInput)).perform(typeText(eventoModificadoStr), closeSoftKeyboard());
        // Escribir descripcion
        onView(withId(R.id.eventoDescripcionInput)).perform(typeText(eventoModificadoStr), closeSoftKeyboard());
        // Click en submit
        onView(withId(R.id.submitButton)).perform(click());
        Thread.sleep(1000);

        // Comprobamos que el titulo y la descripcion se hayan actualizado bien
        onView(withId(R.id.constraintLayout2)).check(matches(hasDescendant(withText(eventoInsertadoStr + eventoModificadoStr))));

        // Borramos el evento
        onView(withId(R.id.deleteEventoFab)).perform(click());

        Thread.sleep(1000);
    }

}
