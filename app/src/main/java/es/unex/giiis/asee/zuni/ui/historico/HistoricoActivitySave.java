package es.unex.giiis.asee.zuni.ui.historico;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import androidx.lifecycle.ViewModelProvider;


import androidx.lifecycle.ViewModelProvider;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.unex.giiis.asee.zuni.EditEventoActivity;
import es.unex.giiis.asee.zuni.R;
import es.unex.giiis.asee.zuni.adapter.HistoricalAdapter;
import es.unex.giiis.asee.zuni.adapter.HistoricalAdapterListener;
import es.unex.giiis.asee.zuni.countrycodes.CountryCode;
import es.unex.giiis.asee.zuni.geocode.GeoCode;
import es.unex.giiis.asee.zuni.historical.HistoricalNetworkDataSource;
import es.unex.giiis.asee.zuni.historical.HistoricalRepository;
import es.unex.giiis.asee.zuni.historical.HistoricalViewModel;
import es.unex.giiis.asee.zuni.historical.HistoricalViewModelFactory;
import es.unex.giiis.asee.zuni.historical.db.HistoricalMinimalDatabase;
import es.unex.giiis.asee.zuni.historical.model.Historical;
import es.unex.giiis.asee.zuni.historical.model.HistoricalMinimal;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;
import es.unex.giiis.asee.zuni.openweather.GeoCodeNetworkLoaderRunnable;
import es.unex.giiis.asee.zuni.openweather.HistoricalNetworkLoaderRunnable;
import es.unex.giiis.asee.zuni.ubicaciones.Ubicacion;
import es.unex.giiis.asee.zuni.ubicaciones.UbicacionRepository;
import es.unex.giiis.asee.zuni.ubicaciones.db.UbicacionDatabase;
import es.unex.giiis.asee.zuni.ui.ubicaciones.UbicacionesFragmentViewModel;
import es.unex.giiis.asee.zuni.ui.ubicaciones.UbicacionesFragmentViewModelFactory;

public class HistoricoActivitySave extends AppCompatActivity {
    private EditText et_city;
    private Spinner spinner1, spinner2;
    private TextView tv_city;
    private Button buttonSearch1, buttonSerach2;
    private Button buttonSave1, buttonSave2;
    //private Button goBackButton;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ProgressBar mProgressBar;
    private HistoricalAdapter mAdapter;


    //Variables for GeoCode API
    private String cityname = null, countrycode = null;

    //Variable del historico
    private Historical histoData = null;
    private boolean isRecovered;

    //Variable para la carga de Room de las ubicaciones
    static List<Ubicacion> ubis = null;
    ArrayAdapter<Ubicacion> spinnerAdapter;


    //Repository Variable
    //private HistoricalRepository mRepository;

    //ViewModel Variable
    private HistoricalViewModel mViewModelHisto;
    private HistoricalViewModelFactory mFactoryHisto;

    //REPOSITORIO UBICACIONES
    //private UbicacionRepository mRepositoryUbi;
    // ViewModel
    private UbicacionesFragmentViewModel mViewModelUbi;
    // Factory
    private UbicacionesFragmentViewModelFactory mFactoryUbi;







    //********************************* BUTTON ACTIONS *********************************

    // Hacer funcionalidad del boton de buscar por nombre

    public void act1(View v){
        //Tell that is running the act1
        //actNum = 1;
        Log.i("Historico ACT1", "Se ha pulsado el boton de buscar historico por nombre");


        //Tell that is recovering data
        isRecovered = false;

        //Get the data from the view
        cityname = et_city.getText().toString().trim();
        countrycode = spinner1.getSelectedItem().toString().substring(0,2);

        Log.i("Historico ACT1", "Se ha pulsado el boto de busqueda de \"" + cityname + "\" en el country \"" + countrycode + "\"");

        if (cityname.equals("")){
            Snackbar.make(v, getString(R.string.Historical_search_err1_msg) + " " + cityname, Snackbar.LENGTH_SHORT).show();
            return;
        }

        loadHistorical(cityname.trim().replace(" ","%20"), countrycode);

        Snackbar.make(v, getString(R.string.Historical_search_msg1) + " " + cityname, Snackbar.LENGTH_SHORT).show();
    }









    // Hacer funcionalidad del boton de buscar por ubicacion
    public void act2(View v){
        //Tell that is running act2
        //actNum = 2;
        Log.i("Historico ACT2", "Se ha pulsado el boton de buscar historico por ubicacion");


        //Tell that is recovering data
        isRecovered = false;

        //Get the location data from the spiner selected location
        Ubicacion seleccionada = (Ubicacion) spinner2.getSelectedItem();
        //seleccion = seleccionada.getUbicacion();

        cityname = seleccionada.getUbicacion();
        countrycode = "ES"; //BY DEFAULT


        loadHistorical(cityname.trim().replace(" ","%20"), countrycode);

        Snackbar.make(v, getString(R.string.Historical_search_msg1) + " " + cityname, Snackbar.LENGTH_SHORT).show();
    }










    // Hacer funcionalidad del boton de guardar
    public void act3(View v){
        //Perform saving
        Log.i("Historico", "Se ha pulsado el boto de guardar el historico");


        //Check if historical was recovered correctly
        if (!isRecovered){
            Snackbar.make(v, getString(R.string.Historical_save_err1_msg), Snackbar.LENGTH_SHORT).show();
            return;
        }

        HistoricalMinimal hm = new HistoricalMinimal();
        hm.initFromHistorical(histoData, cityname, countrycode);

        Intent i = new Intent();
        i.putExtra("Historico", hm);
        setResult(RESULT_OK, i);
        finish();
    }


   // Hacer funcionalidad del boton de cancelar
   public void act4(View v){
       //Abort saving
       Log.i("Historico", "Se ha pulsado el boton de cancelar");

       setResult(RESULT_CANCELED);
       finish();
   }





   public void loadHistorical(String cityname, String countrycode){
        mAdapter.clear();
        isRecovered = false;
        mProgressBar.setVisibility(View.VISIBLE);
        //mRepository.setCity(cityname, countrycode);
       mViewModelHisto.setNetworkCity(cityname, countrycode);
   }


    public void onHistoricalLoaded(Historical histo){
        ArrayList<Historical> histoList = new ArrayList<Historical>();
        histoList.add(histo);

        runOnUiThread(() -> {
            if (cityname == null){ //Si no se ha buscado nada, borrar la cache existente
                mAdapter.clear();
                return;
            }
            mAdapter.swap(histoList);
            mProgressBar.setVisibility(View.GONE);

            histoData = histo;

            String tmp = cityname + ", " + countrycode;
            tv_city.setText(tmp);

            isRecovered = true; //Indicar que se ha recuperado el dato

            //Snackbar.make(myView, getString(R.string.Historical_search_msg1) + " " + cityname, Snackbar.LENGTH_SHORT).show();
        });
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historico_save);
        et_city = findViewById(R.id.et_city);
        spinner1 = findViewById(R.id.spinner1);
        spinner2 = findViewById(R.id.spinner2);
        tv_city = findViewById(R.id.tv_city);
        buttonSearch1 = findViewById(R.id.buttonSearch1);
        buttonSerach2 = findViewById(R.id.buttonSearch2);
        buttonSave1 = findViewById(R.id.buttonSave1);
        buttonSave2 = findViewById(R.id.buttonSave2);
        mProgressBar = findViewById(R.id.progressBar5);
        //goBackButton = findViewById(R.id.GoBackButton);

        /* -------------------------------------------------------------------------------------- */
        /* REPOSITORY UBICACION --------------------------------------------------------------------------- */
        /*mRepositoryUbi = UbicacionRepository.getInstance(UbicacionDatabase.getInstance(this).getDao());
        LiveData<List<Ubicacion>> liveDataUbi = mRepositoryUbi.getUbicaciones();
        liveDataUbi.observe(HistoricoActivitySave.this, new Observer<List<Ubicacion>>() {
            @Override
            public void onChanged(List<Ubicacion> ubicacions) {
                ubis = ubicacions;
                cargarSpinner();
            }
        });
        */
        /* VIEW MODEL --------------------------------------------------------------------------- */

        // Instanciar el factory
        mFactoryUbi = new UbicacionesFragmentViewModelFactory(UbicacionRepository
                .getInstance(UbicacionDatabase.getInstance(this).getDao()));

        // Instanciar el ViewModel
        mViewModelUbi = new ViewModelProvider(this, mFactoryUbi)
                .get(UbicacionesFragmentViewModel.class);

        // Observar los eventos del ViewModel
        mViewModelUbi.getUbicaciones().observe(this, ubicaciones -> {
            ubis=ubicaciones;
            cargarSpinner();
        });
        /* -------------------------------------------------------------------------------------- */
        /* -------------------------------------------------------------------------------------- */

        isRecovered = false; //Indicar que aun no se ha recuperado ningun dato

        //Initialize Repository
        //mRepository = HistoricalRepository.getInstance(HistoricalMinimalDatabase.getInstance(this).getDao(), HistoricalNetworkDataSource.getInstance());
        //Activate observe form API data
        //mRepository.getCurrentHistorical().observe(this, this::onHistoricalLoaded);

        //Initialize Factory
        mFactoryHisto = new HistoricalViewModelFactory(HistoricalRepository.getInstance(HistoricalMinimalDatabase.getInstance(this).getDao(), HistoricalNetworkDataSource.getInstance()));
        //Initialize ViewModel
        mViewModelHisto = new ViewModelProvider(this, mFactoryHisto).get(HistoricalViewModel.class);
        //Activate observe from ViewModel
        mViewModelHisto.getNetworkHistorical().observe(this, this::onHistoricalLoaded);



        //Inicializar Recycler View
        recyclerView = findViewById(R.id.listHistorical);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getBaseContext());
        recyclerView.setLayoutManager(layoutManager);


        //Initialize adapter
        mAdapter = new HistoricalAdapter(new ArrayList<Historical>(), new HistoricalAdapterListener() {
            @Override
            public void imageButtonViewOnClick(View v, int position) {
                //Aqui no se debe hacer nada porque el element no esta en la base de datos (sino que se va a añadir a ella)
                Snackbar.make(v, getText(R.string.Historical_remove_err_msg), Snackbar.LENGTH_SHORT).show();
            }
        });
        recyclerView.setAdapter(mAdapter);


        //Snackbar.make(root, "Surprise :D", Snackbar.LENGTH_SHORT).show();

        buttonSearch1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // LLamar acción del botón 1
                act1(v); //Buscar por nombre de ciudad y countrycode
            }
        });


        buttonSerach2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                act2(v); //Buscar por localización guardada
            }
        });


        buttonSave1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                act3(v); //Guardar el historico actual
            }
        });


        buttonSave2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                act4(v); //Cancelar guardado e irse a la pantalla anterior
            }
        });






        //Cargar el spinner de CountryCodes
        JsonReader reader = new JsonReader(new InputStreamReader(getResources().openRawResource(R.raw.country_codes)));
        List<CountryCode> countryCodes = Arrays.asList(new Gson().fromJson(reader, CountryCode[].class));
        ArrayAdapter<CountryCode> spinnerAdapter = new ArrayAdapter<>(this,R.layout.spinner_item,countryCodes);
        spinner1.setAdapter(spinnerAdapter);
        spinner1.setSelection(208);


        //Cargar la lista de ubicaciones guardadas
        loadItems();



        Log.i("Historico Save", "Se ha cargado el Activity de guardar un historico");
    }


    //********************************* CARGA DEL SPINER DE UBICACIONES *********************************
    // Hacer funciones de carga del spinner
    public void cargarSpinner(){
        spinnerAdapter =
                new ArrayAdapter(this,  R.layout.spinner_item, ubis);
        spinnerAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner2.setAdapter(spinnerAdapter);
        //act2();
    }


    private void loadItems() {
        Context mainCont = this;

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                //ubis = UbicacionDatabase.getInstance(mainCont).getDao().getAll();
                //AppExecutors.getInstance().mainThread().execute(() -> cargarSpinner());
            }
        });
    }
}