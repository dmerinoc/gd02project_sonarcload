package es.unex.giiis.asee.zuni.adapter;

import android.view.View;

public interface HistoricalAdapterListener {
    void imageButtonViewOnClick(View v, int position);
}
