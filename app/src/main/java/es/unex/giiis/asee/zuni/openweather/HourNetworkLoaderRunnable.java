package es.unex.giiis.asee.zuni.openweather;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.zuni.api.daily.DatumMinimal;
import es.unex.giiis.asee.zuni.api.hourlies.HourMinimal;
import es.unex.giiis.asee.zuni.api.hourlies.Hours;
import es.unex.giiis.asee.zuni.ubicaciones.Ubicacion;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HourNetworkLoaderRunnable implements Runnable {

    private final OnHourLoadedListener mOnHourLoadedListener;
    private String city, country;

    public HourNetworkLoaderRunnable(OnHourLoadedListener mOnHourLoadedListener, String city, String country) {
        this.mOnHourLoadedListener = mOnHourLoadedListener;
        this.city = city;
        this.country = country;
    }

    @Override
    public void run() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org/data/2.5/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        OpenWeatherMapService service = retrofit.create(OpenWeatherMapService.class);

        try {
            if (city != null && country != null) {
                Log.e("CITY_Y_COUNTRY", city.concat(",").concat(country));
                Hours hours = service.listHourlies(city.concat(",").concat(country), "55ab2d28aad932680b93bf96e8e44f6e").execute().body();

                if (hours != null) {
                    if (hours.getList() != null) {
                        List<HourMinimal> listHourMinimal = new ArrayList<>();
                        for (int i = 0; i < hours.getList().size(); i++) {
                            HourMinimal d = new HourMinimal();
                            d.initFromMainDaily(hours, i);
                            listHourMinimal.add(d);
                        }
                        AppExecutors.getInstance().mainThread().execute(() -> mOnHourLoadedListener.onHourlyLoaded(listHourMinimal));

                    }
                }
            }
            else{
                AppExecutors.getInstance().mainThread().execute(() -> mOnHourLoadedListener.onHourlyLoaded(new ArrayList<>()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
