package es.unex.giiis.asee.zuni.ui.historico;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import androidx.lifecycle.ViewModelProvider;


import androidx.lifecycle.ViewModelProvider;


import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.unex.giiis.asee.zuni.EditEventoActivity;
import es.unex.giiis.asee.zuni.R;
import es.unex.giiis.asee.zuni.adapter.HistoricalAdapter;
import es.unex.giiis.asee.zuni.adapter.HistoricalAdapterListener;
import es.unex.giiis.asee.zuni.countrycodes.CountryCode;
import es.unex.giiis.asee.zuni.geocode.GeoCode;
import es.unex.giiis.asee.zuni.historical.HistoricalNetworkDataSource;
import es.unex.giiis.asee.zuni.historical.HistoricalRepository;
import es.unex.giiis.asee.zuni.historical.HistoricalViewModel;
import es.unex.giiis.asee.zuni.historical.HistoricalViewModelFactory;
import es.unex.giiis.asee.zuni.historical.model.Historical;
import es.unex.giiis.asee.zuni.historical.model.HistoricalMinimal;
import es.unex.giiis.asee.zuni.historical.db.HistoricalMinimalDatabase;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;
import es.unex.giiis.asee.zuni.ubicaciones.Ubicacion;
import es.unex.giiis.asee.zuni.ubicaciones.UbicacionRepository;
import es.unex.giiis.asee.zuni.ubicaciones.db.UbicacionDatabase;
import es.unex.giiis.asee.zuni.ui.ubicaciones.UbicacionesFragmentViewModel;
import es.unex.giiis.asee.zuni.ui.ubicaciones.UbicacionesFragmentViewModelFactory;

@SuppressWarnings("ALL")
public class HistoricoFragment extends Fragment {
    private static final int REQUEST_SAVE_RESULT = 1;

    private EditText EditText_city;
    private Spinner spinner1, spinner2;
    private Button button1, button2;
    FloatingActionButton buttonAdd;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    //Variables for GeoCode API
    private String cityname, countrycode;

    //Variables for OpenWheather API
    private ArrayList<Historical> histoList;

    //Variable del Historical Adapter
    private HistoricalAdapter mAdapter;

    //Variable para la carga de Room de las ubicaciones
    static List<Ubicacion> ubis = null;
    //private static String seleccion;
    ArrayAdapter<Ubicacion> spinnerAdapter;



    //Repository Variable
    //private HistoricalRepository mRepository;

    //ViewModel Variable
    private HistoricalViewModel mViewModelHisto;
    private HistoricalViewModelFactory mFactoryHisto;

    //REPOSITORIO UBICACIONES
    //private UbicacionRepository mRepositoryUbi;
    // ViewModel
    private UbicacionesFragmentViewModel mViewModelUbi;
    // Factory
    private UbicacionesFragmentViewModelFactory mFactoryUbi;

    //********************************* CARGA DEL SPINER DE UBICACIONES *********************************
    public void cargarSpinner(){
        spinnerAdapter = new ArrayAdapter(getContext(),  R.layout.spinner_item, ubis);
        spinnerAdapter.setDropDownViewResource(R.layout.spinner_item);
        spinner2.setAdapter(spinnerAdapter);
    }


    private void loadSpinnerItems() {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                //ubis = UbicacionDatabase.getInstance(getActivity()).getDao().getAll();
                //AppExecutors.getInstance().mainThread().execute(() -> cargarSpinner());
            }
        });
    }







    //********************************* BUTTON ACTIONS *********************************
    private void act1(){
        //Get the data from the view
        cityname = EditText_city.getText().toString().trim();
        countrycode = spinner1.getSelectedItem().toString().substring(0,2);

        Log.i("Historico", "Se ha pulsado el boto de busqueda de \"" + cityname + "\" en el country \"" + countrycode + "\"");

        if (cityname.equals("")){
            Snackbar.make(getView(), getString(R.string.Historical_search_err1_msg) + " " + cityname, Snackbar.LENGTH_SHORT).show();
            return;
        }


        //Get data from Room
        loadHistoricals(cityname);
    }








    private void act2(){
        if (spinner2.getSelectedItem() == null){
            Snackbar.make(getView(), getString(R.string.Historical_search_err2_msg), Snackbar.LENGTH_SHORT).show();
            return;
        }


        //Get the location data from the spiner selected location
        Ubicacion seleccionada = (Ubicacion) spinner2.getSelectedItem();
        cityname = seleccionada.getUbicacion();
        countrycode = "";



        Log.i("Historico", "Se ha pulsado el boto de busqueda de \"" + cityname + "\" en el Spinner");


        //Get data from Room
        loadHistoricals(cityname);
    }







    private void act3(View v){
        //Se invoca la nueva pantalla y se añade el dato
        Intent i = new Intent(getActivity(), HistoricoActivitySave.class);
        startActivityForResult(i, REQUEST_SAVE_RESULT);
    }







    private void loadHistoricals(String cityname){
        mAdapter.clear();
        //mRepository.setRoomCity(cityname);
        mViewModelHisto.setRoomCity(cityname);
    }


    private void onHistoricalsLoaded(List<Historical> hl){
        //TODO Terminar
        getActivity().runOnUiThread(() -> {
            if (cityname == null){ //Si no se ha buscado nada, borrar la cache existente
                mAdapter.clear();
                return;
            }

            if (hl == null){
                Snackbar.make(getView(), getString(R.string.Historical_search_err2_msg), Snackbar.LENGTH_SHORT).show();
                Log.e("Historico", "No se ha devuelto un historico valido");
                return;
            }
            Log.i("Historico", "Se ha devuelto un historico valido");

            mAdapter.swap(hl);
            histoList = (ArrayList<Historical>) hl;

            Snackbar.make(getView(), getString(R.string.Historical_search_msg1) + " " + cityname, Snackbar.LENGTH_SHORT).show();
        });
    }






    //********************************* ACTIVITY RESULT *********************************

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (REQUEST_SAVE_RESULT == requestCode){
            if (Activity.RESULT_OK == resultCode){
                Log.i("ASD", "Es el request code y el result code");

                HistoricalMinimal hm = (HistoricalMinimal) data.getSerializableExtra("Historico");
                //saveRoomItem(hm);
                //mRepository.insertRoomHistorical(hm);
                mViewModelHisto.insertRoomHistorical(hm);

                Snackbar.make(getView(), getString(R.string.Historical_save_ok_msg), Snackbar.LENGTH_SHORT).show();
            }
            else {
                Log.i("ASD", "Es el request code pero no result code");
                Snackbar.make(getView(), getString(R.string.Historical_save_cancel_msg), Snackbar.LENGTH_SHORT).show();
            }
        }
        else {
            Log.i("ASD", "No es el request code");
            super.onActivityResult(requestCode, resultCode, data);
        }
    }






    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_historico, container, false);
        EditText_city = root.findViewById(R.id.et_city);
        spinner1 = root.findViewById(R.id.spinner1);
        spinner2 = root.findViewById(R.id.spinner2);
        button1 = root.findViewById(R.id.button1);
        button2 = root.findViewById(R.id.button2);
        buttonAdd = root.findViewById(R.id.addHistorical);

        /* -------------------------------------------------------------------------------------- */
        /* REPOSITORY UBICACION --------------------------------------------------------------------------- */
        /*mRepositoryUbi = UbicacionRepository.getInstance(UbicacionDatabase.getInstance(getActivity()).getDao());

        LiveData<List<Ubicacion>> liveDataUbi = mRepositoryUbi.getUbicaciones();
        liveDataUbi.observe(this, new Observer<List<Ubicacion>>() {
            @Override
            public void onChanged(List<Ubicacion> ubicacions) {
                ubis = ubicacions;
                cargarSpinner();
            }
        });*/

        /* VIEW MODEL --------------------------------------------------------------------------- */

        // Instanciar el factory
        mFactoryUbi = new UbicacionesFragmentViewModelFactory(UbicacionRepository
                .getInstance(UbicacionDatabase.getInstance(getContext()).getDao()));

        // Instanciar el ViewModel
        mViewModelUbi = new ViewModelProvider(this, mFactoryUbi)
                .get(UbicacionesFragmentViewModel.class);

        // Observar los eventos del ViewModel
        mViewModelUbi.getUbicaciones().observe(this, ubicaciones -> {
            ubis=ubicaciones;
            cargarSpinner();
        });
        /* -------------------------------------------------------------------------------------- */
        /* -------------------------------------------------------------------------------------- */

        //Cargar el spinner de CountryCodes
        JsonReader reader = new JsonReader(new InputStreamReader(getResources().openRawResource(R.raw.country_codes)));
        List<CountryCode> countryCodes = Arrays.asList(new Gson().fromJson(reader, CountryCode[].class));
        ArrayAdapter<CountryCode> spinnerAdapter = new ArrayAdapter<>(getContext(),R.layout.spinner_item,countryCodes);
        spinner1.setAdapter(spinnerAdapter);
        spinner1.setSelection(208);


        //Initialize Repository
        //mRepository = HistoricalRepository.getInstance(HistoricalMinimalDatabase.getInstance(getActivity()).getDao(), HistoricalNetworkDataSource.getInstance());
        //Activate observe form API data
        //mRepository.getCurrentRoomHistoricals().observe(this, this::onHistoricalsLoaded);

        //Initialize Factory
        mFactoryHisto = new HistoricalViewModelFactory(HistoricalRepository.getInstance(HistoricalMinimalDatabase.getInstance(getActivity()).getDao(), HistoricalNetworkDataSource.getInstance()));
        //Initialize ViewModel
        mViewModelHisto = new ViewModelProvider(this, mFactoryHisto).get(HistoricalViewModel.class);
        //Activate observe from ViewModel
        mViewModelHisto.getRoomHistorical().observe(this, this::onHistoricalsLoaded);


        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Historico", "Se ha pulsado el boton de buscar historico segun el nombre de la ciudad");
                act1(); //Busqueda del historico de "ayer" de la ciudad (indicada en city) y se guarda en la base de datos
            }
        });


        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Historico", "Se ha pulsado el boton de buscar historico de la localización guardada");
                act2(); //Busqueda del historico de "ayer" de la ciudad guardada (indicada en spinner2) y se guarda en la base de datos
            }
        });


        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("Historico", "Se ha pulsado el boton de añadir un nuevo historico");
                act3(v);
            }
        });



        //Se carga la lista de ubicaciones guardadas
        //loadSpinnerItems();


        //Inicializar el RecyclerView
        Log.i("Historico","Se inicializa el recycler view");
        recyclerView = (RecyclerView) root.findViewById(R.id.listHistorical);
        recyclerView.setHasFixedSize(true); //esto hay que ponerlo siempre (no se pa que)
        layoutManager = new LinearLayoutManager(root.getContext());
        recyclerView.setLayoutManager(layoutManager);


        //Inicializar Adapter
        mAdapter = new HistoricalAdapter(new ArrayList<Historical>(), new HistoricalAdapterListener() {
            @Override
            public void imageButtonViewOnClick(View v, int position) {
                //Borrar el historico por posicion
                Snackbar.make(v, getText(R.string.Historical_remove_msg) + " " + position, Snackbar.LENGTH_SHORT).show();

                Integer dt = histoList.get(position).getCurrent().getDt();
                //deleteRoomItem(cityname, dt);
                //mRepository.deleteRoomHistorical(cityname, dt);
                mViewModelHisto.deleteRoomHistorical(cityname, dt);
            }
        });
        recyclerView.setAdapter(mAdapter);



        Log.i("Historico", "Se ha cargado el fragment del historico");
        return root;
    }
}