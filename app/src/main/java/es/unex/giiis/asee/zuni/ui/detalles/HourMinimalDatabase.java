package es.unex.giiis.asee.zuni.ui.detalles;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import es.unex.giiis.asee.zuni.api.hourlies.HourMinimal;

@Database(entities = {HourMinimal.class}, version = 1, exportSchema = false)
public abstract class HourMinimalDatabase extends RoomDatabase {
    private static HourMinimalDatabase INSTANCE;

    public synchronized static HourMinimalDatabase getInstance(Context context) {
        if (INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context, HourMinimalDatabase.class, "hourlies.db").build();
        }
        return INSTANCE;
    }

    public abstract HourMinimalDao hourMinimalDao();
}
