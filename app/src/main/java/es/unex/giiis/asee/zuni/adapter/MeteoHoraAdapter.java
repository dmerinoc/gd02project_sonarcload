package es.unex.giiis.asee.zuni.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import es.unex.giiis.asee.zuni.R;
import es.unex.giiis.asee.zuni.api.hourlies.HourMinimal;

public class MeteoHoraAdapter extends RecyclerView.Adapter<es.unex.giiis.asee.zuni.adapter.MeteoHoraAdapter.MyViewHolder> {
    private List<HourMinimal> mDataset;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView image;
        public TextView hora;

        public TextView descripcion;
        public TextView humedad;
        public TextView viento;
        public TextView dia;
        public View divider;
        public TextView st;
        public TextView temp, tv_donde;
        public View mView;

        public HourMinimal mItem;

        public MyViewHolder(View v) {
            super(v);
            mView = v;

            image = v.findViewById(R.id.h_image);
            hora = v.findViewById(R.id.h_hora);

            descripcion = v.findViewById(R.id.h_descripcion);
            humedad = v.findViewById(R.id.h_humedad);
            viento = v.findViewById(R.id.h_viento);

            st = v.findViewById(R.id.h_st);
            temp = v.findViewById(R.id.h_temp);

            divider = v.findViewById(R.id.divider);

            dia = v.findViewById(R.id.textViewDia);
            tv_donde = v.findViewById(R.id.textViewDonde);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public MeteoHoraAdapter(ArrayList<HourMinimal> myDataset) {
        mDataset = myDataset;
    }


    @Override
    public es.unex.giiis.asee.zuni.adapter.MeteoHoraAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                                                            int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_meteohoras, parent, false);

        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.mItem = mDataset.get(position);

        String hhmm = new SimpleDateFormat("HH:mm", Locale.ENGLISH).format(new Date((holder.mItem.getDt()) * 1000l));

        if (position == 0
                ||"00:00".equals(hhmm)
                || "01:00".equals(hhmm)
                || "02:00".equals(hhmm)
        )
        {
            holder.divider.setVisibility(View.VISIBLE);
            holder.dia.setVisibility(View.VISIBLE);
            holder.dia.setText(new SimpleDateFormat("E, dd MMM yyyy", Locale.ENGLISH)
                    .format(new Date((holder.mItem.getDt()) * 1000)));
            if(position == 0){
                holder.tv_donde.setVisibility(View.VISIBLE);
                holder.tv_donde.setText(holder.mItem.getCity().concat(", ").concat(holder.mItem.getCountryCode()));
            }
            else{
                holder.tv_donde.setVisibility(View.GONE);
            }
        }
        else{
            holder.divider.setVisibility(View.GONE);
            holder.dia.setVisibility(View.GONE);
            holder.tv_donde.setVisibility(View.GONE);
        }


        switch (holder.mItem.getMain()) {
            case "Thunderstorm":
                holder.image.setImageResource(R.drawable.tormenta);
                break;
            case "Drizzle":
            case "Rain":
                holder.image.setImageResource(R.drawable.lluvia);
                break;
            case "Snow":
                holder.image.setImageResource(R.drawable.nieve);
                break;
            case "Clear":
                holder.image.setImageResource(R.drawable.sol);
                break;
            case "Clouds":
                holder.image.setImageResource(R.drawable.nube);
                break;
            case "Mist":
                holder.image.setImageResource(R.drawable.niebla);
        }

        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.');

        DecimalFormat df = new DecimalFormat("0", simbolos);
        DecimalFormat df1 = new DecimalFormat("0.0", simbolos);
        DecimalFormat df2 = new DecimalFormat("0.00", simbolos);

        holder.hora.setText(new SimpleDateFormat("HH:mm", Locale.ENGLISH)
                .format(new Date((holder.mItem.getDt()) * 1000l)));

        String input = holder.mItem.getDescription();
        String output = input.substring(0, 1).toUpperCase() + input.substring(1);
        holder.descripcion.setText(output);

        holder.humedad.setText(df.format(holder.mItem.getPop() * 100).concat("%"));

        holder.viento.setText(df2.format(holder.mItem.getWindSpeed() * 1).concat(" m/s"));
        holder.st.setText(df2.format(holder.mItem.getFeelsLike() - 273).concat(" ºC"));
        holder.temp.setText(df2.format(holder.mItem.getTemp() - 273).concat(" ºC"));
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    public void clear() {
        mDataset.clear();
        notifyDataSetChanged();
    }

    public void swap(List<HourMinimal> dataset) {
        if (dataset == null) {
            mDataset = new ArrayList<HourMinimal>();
        } else {
            if (dataset.size() > 0)
                mDataset = dataset;
            else mDataset = new ArrayList<HourMinimal>();
        }

        notifyDataSetChanged();
    }
}