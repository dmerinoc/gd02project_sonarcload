package es.unex.giiis.asee.zuni.ui.detalles;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

/**
 * Factory method that allows us to create a ViewModel with a constructor that takes a
 * {@link HourMinimalRepository}
 */
public class DetallesFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final HourMinimalRepository mRepository;

    public DetallesFragmentViewModelFactory(HourMinimalRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new DetallesFragmentViewModel(mRepository);
    }
}
