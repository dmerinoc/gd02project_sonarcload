package es.unex.giiis.asee.zuni.eventos;

import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.unex.giiis.asee.zuni.eventos.db.EventoDao;
import es.unex.giiis.asee.zuni.eventos.db.EventoDatabase;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;

public class EventoRepository {
    private static final String LOG_TAG = EventoRepository.class.getSimpleName();

    // For Singleton instantiation
    private static EventoRepository sInstance;
    private final EventoDao mEventoDao;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    // private final MutableLiveD
    private final Map<String, Long> lastUpdateTimeMillisMap = new HashMap<>();
    private static final long MIN_TIME_FROM_LAST_FETCH_MILLIS = 30000;


    /* CONSTRUCTOR ------------------------------------------------------------------------------ */
    private EventoRepository(EventoDao eventoDao){
        mEventoDao = eventoDao;

        // LiveData que obtiene los eventos de la BD
        LiveData<List<Evento>> dbData = eventoDao.getAll();

        // As long as the repository exists, observe the db LiveData.
        dbData.observeForever(newEventosFromDB -> {
            mExecutors.diskIO().execute(() -> {

                if (dbData.getValue().size() == 0){
                    // Insertamos los nuevos eventos en la BD
                    mEventoDao.bulkInsert(newEventosFromDB);
                    Log.d(LOG_TAG, "New values inserted in Room");
                }
            });
        });
    }


    /* INSTANCIA SINGLETON ---------------------------------------------------------------------- */
    public synchronized static EventoRepository getInstance(EventoDao dao){
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null){
            sInstance = new EventoRepository(dao);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }


    /*
    * ----------------------------------------------------------------------------------------------
    * OPERACIONES DE LA BASE DE DATOS
    * ----------------------------------------------------------------------------------------------
    */


    /* GET EVENTO ------------------------------------------------------------------------------- */
    public Evento getEvento(long id){
        Log.d(LOG_TAG, "OBTENIENDO EVENTO " + id);
        return mEventoDao.getEvento(id);
    }

    /* GET EVENTOS ------------------------------------------------------------------------------ */
    public LiveData<List<Evento>> getEventos(){
        Log.d(LOG_TAG, "OBTENIENDO EVENTOS");
        return mEventoDao.getAll();
    }


    /* DELETE EVENTO ---------------------------------------------------------------------------- */
    public void deleteEvento(Evento evento){
        AppExecutors.getInstance().diskIO().execute(() -> mEventoDao.delete(evento.getId()));
        Log.d(LOG_TAG, "BORRANDO EVENTO " + evento.getTitulo());
    }


    /* INSERT EVENTO ---------------------------------------------------------------------------- */
    public long insertEvento(Evento evento){
        Log.d(LOG_TAG, "INSERTANDO EVENTO " + evento.getTitulo());
        return mEventoDao.insert(evento);
    }


    /* UPDATE EVENTO ---------------------------------------------------------------------------- */
    public void updateEvento(Evento evento){
        Log.d(LOG_TAG, "EDITANDO EVENTO " + evento.getTitulo());
        mEventoDao.update(evento);
    }

}
