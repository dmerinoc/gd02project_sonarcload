package es.unex.giiis.asee.zuni.eventos.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import static androidx.room.OnConflictStrategy.REPLACE;

import java.util.List;

import es.unex.giiis.asee.zuni.eventos.Evento;

@Dao
public interface EventoDao {

    // NUEVO (Cambiar a LiveData)
    @Query("SELECT * FROM eventos")
    public LiveData<List<Evento>> getAll();

    @Query("SELECT * FROM eventos")
    public List<Evento> getAllEventos();

    @Query("SELECT * FROM eventos WHERE id = :eventoId")
    public Evento getEvento(long eventoId);

    // NUEVO
    @Insert(onConflict = REPLACE)
    void bulkInsert(List<Evento> eventos);

    @Insert
    public long insert(Evento item);

    @Query("DELETE FROM eventos WHERE id = :eventoId")
    public void delete(long eventoId);

    @Query("DELETE FROM eventos")
    public void deleteAll();

    @Update
    public int update(Evento item);
}
