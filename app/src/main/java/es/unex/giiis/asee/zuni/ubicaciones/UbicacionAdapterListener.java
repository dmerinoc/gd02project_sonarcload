package es.unex.giiis.asee.zuni.ubicaciones;

import android.view.View;

public interface UbicacionAdapterListener {
    void deleteButtonOnClick(View v , int position);
    void favButtonOnClick(View v , int position);
}