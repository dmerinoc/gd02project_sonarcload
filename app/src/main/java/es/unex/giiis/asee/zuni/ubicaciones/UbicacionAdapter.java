package es.unex.giiis.asee.zuni.ubicaciones;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.zuni.R;
import es.unex.giiis.asee.zuni.adapter.HistoricalAdapterListener;
import es.unex.giiis.asee.zuni.eventos.Evento;


public class UbicacionAdapter extends RecyclerView.Adapter<UbicacionAdapter.ViewHolder> {
    private List<Ubicacion> mItems = new ArrayList<Ubicacion>();
    private UbicacionAdapterListener listener;
    Context mContext;



    /* CONSTRUCTOR DEL ADAPTER*/
    public UbicacionAdapter(Context context, UbicacionAdapterListener listener){
        this.mContext = context;
        this.listener = listener;
    }


    /* CREA NUEVAS VISTAS (INVOCADO POR EL LAYOUT MANAGER) */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        // Se infla la vista para cada elemento
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.ubicacion, parent, false);

        return new ViewHolder(mContext, v, listener);
    }


    /* REEMPLAZA LOS CONTENIDOS DE UNA VISTA (INVOCADO POR EL LAYOUT MANAGER) */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position){
        holder.bind(mItems.get(position), listener);
    }


    /* DEVUELVE EL NUMERO DE ELEMENTOS DE LA COLECCION */
    @Override
    public int getItemCount(){
        return mItems.size();
    }


    public void add(Ubicacion item){
        mItems.add(item);
        notifyDataSetChanged();
    }

    public void remove(Ubicacion item){
        mItems.remove(item);
        notifyDataSetChanged();
    }

    public void clear() {
        mItems.clear();
        notifyDataSetChanged();
    }

    public void load(List<Ubicacion> items){
        mItems.clear();
        mItems = items;
        notifyDataSetChanged();
    }



    public Object getItem(int pos) { return mItems.get(pos); }


    /* VIEWHOLDER ------------------------------------------------------------------------------- */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        private Context mContext;

        /* Elementos del layout de cada item */
        private TextView TVubicacion;
        private ImageButton favUbicacion;
        private ImageButton deleteUbicacion;


        private WeakReference<UbicacionAdapterListener> listenerRef;


        public ViewHolder(Context context, View itemView, UbicacionAdapterListener listener){
            super(itemView);

            listenerRef = new WeakReference<>(listener);

            mContext = context;

            // Obtener las referencias de cada widget de la vista del elemento
            TVubicacion = itemView.findViewById(R.id.ubicacionUbicacion);
            favUbicacion = itemView.findViewById(R.id.favButtonUbicacion);
            deleteUbicacion = itemView.findViewById(R.id.deleteButtonUbicacion);

            favUbicacion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listenerRef.get().favButtonOnClick(itemView, getAdapterPosition());
                }
            });


            deleteUbicacion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listenerRef.get().deleteButtonOnClick(itemView, getAdapterPosition());
                }
            });
        }

        public void bind(final Ubicacion ubicacion, final UbicacionAdapterListener listener) {

            /* Configurar los widgets del elemento */

            if(ubicacion.getBanderaUbiFav()) {
                TVubicacion.setText(ubicacion.getUbicacion().concat(" (Ubicación predeterminada)"));
            }else{
                TVubicacion.setText(ubicacion.getUbicacion());
            }

        }
    }

    /* SWAP ------------------------------------------------------------------------------------- */
    public void swap(List<Ubicacion> dataset){
        mItems = dataset;
        notifyDataSetChanged();
    }

    /* GET DATASET ------------------------------------------------------------------------------ */
    public List<Ubicacion> getDataset(){
        return this.mItems;
    }
}