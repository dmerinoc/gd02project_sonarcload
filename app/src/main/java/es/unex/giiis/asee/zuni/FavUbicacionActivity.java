package es.unex.giiis.asee.zuni;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import java.util.List;

import es.unex.giiis.asee.zuni.openweather.AppExecutors;
import es.unex.giiis.asee.zuni.ubicaciones.Ubicacion;
import es.unex.giiis.asee.zuni.ubicaciones.UbicacionRepository;
import es.unex.giiis.asee.zuni.ubicaciones.db.UbicacionDatabase;
import es.unex.giiis.asee.zuni.ui.ubicaciones.UbicacionesFragmentViewModel;
import es.unex.giiis.asee.zuni.ui.ubicaciones.UbicacionesFragmentViewModelFactory;


public class FavUbicacionActivity extends AppCompatActivity {

    private Ubicacion ubicacion;

    private static final String TAG = "Zuni-FavUbicacion";
    private TextView mUbicacionFav;
    //private boolean mbanderaUbiFav;
    private List<Ubicacion> ubicacionList;

    //Repositorio
    //private UbicacionRepository mRepositoryUbi;
    // ViewModel
    private UbicacionesFragmentViewModel mViewModelUbi;
    // Factory
    private UbicacionesFragmentViewModelFactory mFactoryUbi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav_ubicacion);

        mUbicacionFav = findViewById(R.id.ubicacionfav);

        /* -------------------------------------------------------------------------------------- */
        /* REPOSITORY UBICACION --------------------------------------------------------------------------- */
        /*mRepositoryUbi = UbicacionRepository.getInstance(UbicacionDatabase.getInstance(this).getDao());
        LiveData<List<Ubicacion>> liveDataUbi = mRepositoryUbi.getUbicaciones();
        liveDataUbi.observe(FavUbicacionActivity.this, new Observer<List<Ubicacion>>() {
                    @Override
                    public void onChanged(List<Ubicacion> ubicacions) {
                        ubicacionList=ubicacions;
                    }
                });
        */

        /* VIEW MODEL --------------------------------------------------------------------------- */

        // Instanciar el factory
        mFactoryUbi = new UbicacionesFragmentViewModelFactory(UbicacionRepository
                .getInstance(UbicacionDatabase.getInstance(this).getDao()));

        // Instanciar el ViewModel
        mViewModelUbi = new ViewModelProvider(this, mFactoryUbi)
                .get(UbicacionesFragmentViewModel.class);

        // Observar los eventos del ViewModel
        mViewModelUbi.getUbicaciones().observe(this, ubicaciones -> {
            ubicacionList=ubicaciones;
        });
                /* -------------------------------------------------------------------------------------- */
                /* -------------------------------------------------------------------------------------- */

        /* SE OBTIENE LA UBICACION DEL INTENT */
        ubicacion = new Ubicacion(getIntent());
        Ubicacion ubicacionIntent = new Ubicacion(getIntent());

        /* SE OBTIENE LA UBICACION DE LA BD  */
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {

                //HECHO CON REPOSITORY
                //ubicacion = mRepositoryUbi.getUbicacion(ubicacionIntent.getId());
                ubicacion = mViewModelUbi.getUbicacion(ubicacionIntent.getId());

                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        /* SE CARGAN LOS ATRIBUTOS DE LA UBICACION EN LOS INPUTS --------------------- */
                        Log.e("FAVUBI","ubicacion" + ubicacion);
                        Log.e("FAVUBI","Nombre" + ubicacion.getUbicacion());
                        mUbicacionFav.setText(ubicacion.getUbicacion());
                    }
                });
            }
        });

        /* Listener para el boton de CANCEL ----------------------------------------------------- */
        final Button cancelButton = findViewById(R.id.no_favUbicacion);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent();
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        /* Listener para el boton de AÑADIR UBICACION FAV ----------------------------------------------------- */
        final Button okeyButton = findViewById(R.id.yes_favUbicacion);
        okeyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /* Obtener los datos de la ubicacion*/

                /* ACTUALIZAR UBICACIONES COMO NO PREDETERMINADAS */
                /*AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        LiveData<List<Ubicacion>> liveDataUbi = mRepositoryUbi.getUbicaciones();
                        liveDataUbi.observe(FavUbicacionActivity.this, new Observer<List<Ubicacion>>() {
                            @Override
                            public void onChanged(List<Ubicacion> listUbi) {
                                for (int i = 0; i < listUbi.size(); i++) {
                                    if (listUbi.get(i).getBanderaUbiFav()) {
                                        listUbi.get(i).setBanderaUbiFav(false);
                                        UbicacionDatabase.getInstance(FavUbicacionActivity.this)
                                                .getDao().update(listUbi.get(i));
                                    }
                                }
                            }
                        });
                    }
                });
                */
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        for (int i = 0; i < ubicacionList.size(); i++) {
                    if (ubicacionList.get(i).getBanderaUbiFav()) {
                        ubicacionList.get(i).setBanderaUbiFav(false);
                        //mRepositoryUbi.updateUbicacion(ubicacionList.get(i));
                        mViewModelUbi.updateUbicacion(ubicacionList.get(i));
                    }
                }}});

                /* Empaquetar la ubicacion en un intent */
                Intent data = new Intent();
                //y aqui pongo la bandera a true para que se ha dado click y se pasa con el intent
                ubicacion.setBanderaUbiFav(true);
                Ubicacion.packageIntent(data,ubicacion);

                //Ubicacion ubicacionCreada = new Ubicacion(data);

                setResult(RESULT_OK, data);
                finish();
            }
        });
    }


}