package es.unex.giiis.asee.zuni.historical;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import es.unex.giiis.asee.zuni.geocode.GeoCode;
import es.unex.giiis.asee.zuni.historical.model.Historical;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;
import es.unex.giiis.asee.zuni.openweather.GeoCodeNetworkLoaderRunnable;
import es.unex.giiis.asee.zuni.openweather.HistoricalNetworkLoaderRunnable;

public class HistoricalNetworkDataSource {
    private static final String LOG_TAG = HistoricalNetworkDataSource.class.getSimpleName();

    //Singleton
    private static HistoricalNetworkDataSource sInstance;

    //LiveData
    private final MutableLiveData<Historical> mDownloadedHistorical;

    //Private variables
    //private String cityname, countrycode;
    private double lon, lat; //Geocode coordinates
    //private Historical histoData;


    private HistoricalNetworkDataSource() {
        mDownloadedHistorical = new MutableLiveData<>();
    }

    public synchronized static HistoricalNetworkDataSource getInstance(){
        Log.d(LOG_TAG, "Getting the network data source");
        if (sInstance == null) {
            sInstance = new HistoricalNetworkDataSource();
            Log.d(LOG_TAG, "New network data source created");
        }
        return sInstance;
    }


    public LiveData<Historical> getCurrentHistorical() {
        return mDownloadedHistorical;
    }





    public void fetchHistorical(String cityname, String countrycode){
        //this.cityname = cityname;
        //this.countrycode = countrycode;

        //Get coordinates from the GeoCode API
        AppExecutors.getInstance().networkIO().execute(new GeoCodeNetworkLoaderRunnable(
                this::setUbicacion, cityname.replace(" ","%20"), countrycode
        ));
    }

    private void setUbicacion(GeoCode geoCode){
        lat = Double.parseDouble(geoCode.getLatt());
        lon = Double.parseDouble(geoCode.getLongt());
        fhp2(); //Call next function
    }


    private void fhp2(){
        //Get the historical from the API
        long timeS = (System.currentTimeMillis()) / 1000 - 86400; //Carga el timestamp en segundos de ayer
        AppExecutors.getInstance().networkIO().execute(new HistoricalNetworkLoaderRunnable(
                this::setHistorical, lat, lon, timeS
        ));
    }

    private void setHistorical(List<Historical> dataset){
        //histoData = dataset.get(0); //Set the historical variable
        //Set the LiveData with the returned historical
        mDownloadedHistorical.postValue(dataset.get(0));
    }




}
