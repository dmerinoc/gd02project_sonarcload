package es.unex.giiis.asee.zuni.openweather;

import java.util.List;

import es.unex.giiis.asee.zuni.api.current.Current;

public interface OnCurrentLoadedListener {
    public void onCurrentLoaded(List<Current> listCurrent);
}
