package es.unex.giiis.asee.zuni.ui.previsiones;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

/**
 * Factory method that allows us to create a ViewModel with a constructor that takes a
 * {@link DatumMinimalRepository}
 */
public class PrevisionesFragmentViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final DatumMinimalRepository mRepository;

    public PrevisionesFragmentViewModelFactory(DatumMinimalRepository repository) {
        this.mRepository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new PrevisionesFragmentViewModel(mRepository);
    }
}
