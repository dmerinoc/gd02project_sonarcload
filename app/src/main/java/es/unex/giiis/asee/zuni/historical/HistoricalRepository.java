package es.unex.giiis.asee.zuni.historical;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.zuni.historical.db.HistoricalMinimalDAO;
import es.unex.giiis.asee.zuni.historical.db.HistoricalMinimalDatabase;
import es.unex.giiis.asee.zuni.historical.model.Historical;
import es.unex.giiis.asee.zuni.historical.model.HistoricalMinimal;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;
import es.unex.giiis.asee.zuni.ubicaciones.Ubicacion;

public class HistoricalRepository {
    private static final String LOG_TAG = HistoricalRepository.class.getSimpleName();

    //Singleton
    private static HistoricalRepository sInstance;

    //Data Instances
    private final HistoricalMinimalDAO mHistoricalDao;
    private final HistoricalNetworkDataSource mHistoricalNetworkDataSource;

    //AppExecutor Instance
    private final AppExecutors mExecutors = AppExecutors.getInstance();

    //private final MutableLiveData<String> cityFilterLiveData = new MutableLiveData<>();


    //Network API data LiveData
    private final MutableLiveData<Historical> historicalNetworkLiveData = new MutableLiveData<Historical>();

    //Local ROOM data LiveData
    private final MutableLiveData<List<Historical>> historicalRoomLiveData = new MutableLiveData<List<Historical>>();
    private String historicalRoomCityname;



    private HistoricalRepository(HistoricalMinimalDAO historicalDao, HistoricalNetworkDataSource historicalNetworkDataSource){
        //CTor
        mHistoricalDao = historicalDao;
        mHistoricalNetworkDataSource = historicalNetworkDataSource;

        //LiveData that fetches historical from network
        LiveData<Historical> networkData = mHistoricalNetworkDataSource.getCurrentHistorical();

        networkData.observeForever(newHistoricalFromNetwork -> {
            if (newHistoricalFromNetwork != null){
                historicalNetworkLiveData.postValue(newHistoricalFromNetwork);
            }
        });
    }

    public synchronized static HistoricalRepository getInstance(HistoricalMinimalDAO dao, HistoricalNetworkDataSource nds){
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new HistoricalRepository(dao, nds);
            Log.d(LOG_TAG, "New repository created");
        }
        return sInstance;
    }


    /******************************************************
     * OPERACIONES DE LA API (REMOTO)
     ******************************************************/



    public void setCity(final String cityname, final String countrycode){
        //cityFilterLiveData.setValue(cityname);
        mExecutors.diskIO().execute(() -> doFetchHistoricals(cityname, countrycode));
    }


    private void doFetchHistoricals(String cityname, String countrycode){
        Log.d(LOG_TAG, "Fetching Historicals from API");
        mHistoricalNetworkDataSource.fetchHistorical(cityname, countrycode);
        /*mExecutors.diskIO().execute(() -> {
            //Obtain historicals from network
            mHistoricalNetworkDataSource.fetchHistorical(cityname, countrycode);
        });*/
    }


    public LiveData<Historical> getCurrentHistorical(){
        return historicalNetworkLiveData;
    }




    /******************************************************
     * OPERACIONES DE ROOM (LOCAL)
     ******************************************************/


    public void setRoomCity(final String cityname){
        historicalRoomCityname = cityname;
        mExecutors.diskIO().execute(() -> doFetchRoomHistoricals(cityname));
    }


    private void doFetchRoomHistoricals(String cityname){
        List<HistoricalMinimal> tmpList = mHistoricalDao.getHistoricalsByCityname(cityname);
        List<Historical> histoList = new ArrayList<Historical>();

        for (HistoricalMinimal hm : tmpList){
            Historical h;
            h = hm.convertIntoHistorical();

            histoList.add(h);
        }

        historicalRoomLiveData.postValue(histoList);
    }


    public LiveData<List<Historical>> getCurrentRoomHistoricals(){
        return historicalRoomLiveData;
    }


    public void insertRoomHistorical(HistoricalMinimal hm){
        mExecutors.diskIO().execute(() -> {
            mHistoricalDao.insert(hm);
            doFetchRoomHistoricals(historicalRoomCityname); //Recuperar los historicos tras modificar la DB
        });
        //Log.i("Historical", "Se ha insertado un elemento en el historico con el id: " + Long.toString(id));
    }


    public void deleteRoomHistorical(String cityname, Integer dt){
        mExecutors.diskIO().execute(() -> {
            mHistoricalDao.delete(cityname, dt);
            doFetchRoomHistoricals(historicalRoomCityname); //Recuperar los historicos tras modificar la DB
        });
    }


}
