package es.unex.giiis.asee.zuni.openweather;

import java.util.List;

import es.unex.giiis.asee.zuni.historical.model.Historical;

public interface OnHistoricalLoadedListener {
    public void onHistoricalLoaded(List<Historical> listHistorical);
}
