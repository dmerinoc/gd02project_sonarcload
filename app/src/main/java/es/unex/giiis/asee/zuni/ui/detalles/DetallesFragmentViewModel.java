package es.unex.giiis.asee.zuni.ui.detalles;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.zuni.api.hourlies.HourMinimal;

public class DetallesFragmentViewModel extends ViewModel {


    private final HourMinimalRepository mRepository;
    private final LiveData<List<HourMinimal>> mHourMinimals;
    private String mCiudad = "";
    private String mCountryCode = "";

    public DetallesFragmentViewModel(HourMinimalRepository repository) {
        mRepository = repository;
        mHourMinimals = mRepository.getCurrentHourMinimals();
    }

    public void setName(String ciudad, String countryCode){
        if(ciudad!=null && countryCode != null) {
            if (!ciudad.trim().equals("") && !countryCode.trim().equals("")) {
                mCiudad = ciudad;
                mCountryCode = countryCode;
                mRepository.setName(ciudad, countryCode);
            }
        }
    }

    public void onRefresh() {
        mRepository.doFetchHourMinimals(mCiudad,mCountryCode);
    }

    public LiveData<List<HourMinimal>> getHourMinimals() {
        return mHourMinimals;
    }
}
