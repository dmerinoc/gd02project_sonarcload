package es.unex.giiis.asee.zuni.historical;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.zuni.historical.model.Historical;
import es.unex.giiis.asee.zuni.historical.model.HistoricalMinimal;

public class HistoricalViewModel extends ViewModel {
    private final HistoricalRepository mRepository;
    private final LiveData<Historical> mHistoricalNetworkLiveData;
    private final LiveData<List<Historical>> mHistoricalRoomLiveData;

    private String networkCityname;
    private String networkCountrycode;

    private String roomCityname;

    public HistoricalViewModel(HistoricalRepository repository){
        mRepository = repository;
        mHistoricalNetworkLiveData = mRepository.getCurrentHistorical();
        mHistoricalRoomLiveData = mRepository.getCurrentRoomHistoricals();
    }



    /******************************************************
     * OPERACIONES DE LA API (REMOTO)
     ******************************************************/


    public void setNetworkCity(final String cityname, final String countrycode){
        networkCityname = cityname;
        networkCountrycode = countrycode;
        mRepository.setCity(cityname, countrycode);
    }

    public LiveData<Historical> getNetworkHistorical(){
        return mHistoricalNetworkLiveData;
    }





    /******************************************************
     * OPERACIONES DE ROOM (LOCAL)
     ******************************************************/

    public void setRoomCity(final String cityname){
        roomCityname = cityname;
        mRepository.setRoomCity(cityname);
    }


    public LiveData<List<Historical>> getRoomHistorical(){
        return mHistoricalRoomLiveData;
    }




    public void insertRoomHistorical(HistoricalMinimal hm){
        mRepository.insertRoomHistorical(hm);
    }

    public void deleteRoomHistorical(String cityname, Integer dt){
        mRepository.deleteRoomHistorical(cityname, dt);
    }


}
