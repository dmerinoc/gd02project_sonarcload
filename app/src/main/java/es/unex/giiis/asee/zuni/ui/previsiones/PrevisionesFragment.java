package es.unex.giiis.asee.zuni.ui.previsiones;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import es.unex.giiis.asee.zuni.MyApplication;
import es.unex.giiis.asee.zuni.R;
import es.unex.giiis.asee.zuni.adapter.DailyAdapter;
import es.unex.giiis.asee.zuni.api.daily.DatumMinimal;
import es.unex.giiis.asee.zuni.countrycodes.CountryCode;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;
import es.unex.giiis.asee.zuni.ubicaciones.Ubicacion;
import es.unex.giiis.asee.zuni.ubicaciones.UbicacionRepository;
import es.unex.giiis.asee.zuni.ubicaciones.db.UbicacionDatabase;
import es.unex.giiis.asee.zuni.ui.ubicaciones.UbicacionesFragmentViewModel;
import es.unex.giiis.asee.zuni.ui.ubicaciones.UbicacionesFragmentViewModelFactory;

public class PrevisionesFragment extends Fragment {
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private List<CountryCode> countryCodes;
    private ProgressBar mProgressBar;
    private EditText city;
    private DailyAdapter adapter;
    private RecyclerView recyclerView;
    private Spinner spinner, spinner2;
    private Button button, button2;
    private PrevisionesFragmentViewModel mViewModel = null;
    //private UbicacionRepository mRepositoryUbi;

    // ViewModel
    private UbicacionesFragmentViewModel mViewModelUbi;
    // Factory
    private UbicacionesFragmentViewModelFactory mFactoryUbi;

    private void showLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.INVISIBLE);
    }

    private void showDatumMinimalsDataView() {
        mProgressBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
        recyclerView.setVisibility(View.VISIBLE);
    }

    public void cargarSpinner(List<Ubicacion> ubis) {
        if (ubis != null && ubis.size() > 0) {
            ArrayAdapter<Ubicacion> spinnerAdapter =
                    new ArrayAdapter(getContext(), R.layout.spinner_item, ubis);
            spinnerAdapter.setDropDownViewResource(R.layout.spinner_item);
            spinner.setAdapter(spinnerAdapter);
            boolean enc = false;
            for (int i = 0; i < ubis.size() && !enc; i++) {
                if (ubis.get(i).getBanderaUbiFav()) {
                    enc = true;
                    spinner.setSelection(i);
                }
            }
        } else {
            ArrayAdapter<Ubicacion> spinnerAdapter =
                    new ArrayAdapter(getContext(), R.layout.spinner_item, new ArrayList<Ubicacion>());
            spinnerAdapter.setDropDownViewResource(R.layout.spinner_item);
            spinner.setAdapter(spinnerAdapter);
        }
    }


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_previsiones, container, false);
        spinner = (Spinner) root.findViewById(R.id.spinner);
        spinner2 = (Spinner) root.findViewById(R.id.spinner2);
        button = (Button) root.findViewById(R.id.button);
        button2 = (Button) root.findViewById(R.id.button2);
        city = (EditText) root.findViewById(R.id.et_city);
        recyclerView = root.findViewById(R.id.listDaily);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));
        adapter = new DailyAdapter(new ArrayList<DatumMinimal>());
        recyclerView.setAdapter(adapter);
        mSwipeRefreshLayout = root.findViewById(R.id.swipeRefreshLayout);
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // In landscape
            mProgressBar = root.findViewById(R.id.progressBar2);
        } else {
            // In portrait
            mProgressBar = root.findViewById(R.id.progressBar1);
        }

        JsonReader reader = new JsonReader(new InputStreamReader(getResources().openRawResource(R.raw.country_codes)));
        countryCodes = Arrays.asList(new Gson().fromJson(reader, CountryCode[].class));
        ArrayAdapter<CountryCode> spinnerAdapter2 = new ArrayAdapter<>(getContext(), R.layout.spinner_item, countryCodes);
        spinner2.setAdapter(spinnerAdapter2);
        spinner2.setSelection(208);

        PrevisionesContainer previsionesContainer = ((MyApplication) getActivity().getApplication()).appContainerPrevisiones;
        mViewModel = new ViewModelProvider(this, previsionesContainer.factory).get(PrevisionesFragmentViewModel.class);
        mViewModel.getDatumMinimals().observe(getActivity(), datumMinimals -> {
            adapter.swap(datumMinimals);
            if (datumMinimals != null && datumMinimals.size() != 0) showDatumMinimalsDataView();
            else showLoading();
        });

        /*mRepositoryUbi = UbicacionRepository.getInstance(UbicacionDatabase.getInstance(getActivity()).getDao());
        LiveData<List<Ubicacion>> liveDataUbi = mRepositoryUbi.getUbicaciones();
        liveDataUbi.observe(this, new Observer<List<Ubicacion>>() {
            @Override
            public void onChanged(List<Ubicacion> ubicacions) {
                cargarSpinner(ubicacions);
                if((Ubicacion) spinner.getSelectedItem() != null)
                    mViewModel.setName(((Ubicacion) spinner.getSelectedItem()).getUbicacion(), ((Ubicacion) spinner.getSelectedItem()).getCountryCode());
            }
        });*/
        /* VIEW MODEL --------------------------------------------------------------------------- */

        // Instanciar el factory
        mFactoryUbi = new UbicacionesFragmentViewModelFactory(UbicacionRepository
                .getInstance(UbicacionDatabase.getInstance(getActivity()).getDao()));

        // Instanciar el ViewModel
        mViewModelUbi = new ViewModelProvider(this, mFactoryUbi)
                .get(UbicacionesFragmentViewModel.class);

        // Observar los eventos del ViewModel
        mViewModelUbi.getUbicaciones().observe(this, ubicaciones -> {
            cargarSpinner(ubicaciones);
            if((Ubicacion) spinner.getSelectedItem() != null)
                mViewModel.setName(((Ubicacion) spinner.getSelectedItem()).getUbicacion(), ((Ubicacion) spinner.getSelectedItem()).getCountryCode());

        });

        button.setOnClickListener(view -> mViewModel.setName(((Ubicacion) spinner.getSelectedItem()).getUbicacion(), ((Ubicacion) spinner.getSelectedItem()).getCountryCode()));
        button2.setOnClickListener(view -> mViewModel.setName(city.getText().toString().trim(), spinner2.getSelectedItem().toString().substring(0, 2)));
        mSwipeRefreshLayout.setOnRefreshListener(mViewModel::onRefresh);

        return root;
    }
}