package es.unex.giiis.asee.zuni.ubicaciones.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import es.unex.giiis.asee.zuni.ubicaciones.Ubicacion;

import static androidx.room.OnConflictStrategy.REPLACE;


@Dao
public interface UbicacionDao {

    // NUEVO (Cambiar a LiveData)
    @Query("SELECT * FROM ubicaciones")
    public LiveData<List<Ubicacion>> getAll();

    @Query("SELECT * FROM ubicaciones")
    public List<Ubicacion> getAllUbicaciones();

    @Query("SELECT * FROM ubicaciones WHERE id = :ubicacionId")
    public Ubicacion getUbicacion(long ubicacionId);

    //NUEVO
    @Insert(onConflict = REPLACE)
    void bulkInsert(List<Ubicacion> ubicaciones);

    @Insert
    public long insert(Ubicacion item);

    @Query("DELETE FROM ubicaciones WHERE id = :ubicacionId")
    public void delete(long ubicacionId);

    @Query("DELETE FROM ubicaciones")
    public void deleteAll();

    @Update
    public int update(Ubicacion item);
}