package es.unex.giiis.asee.zuni.ui.detalles;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import es.unex.giiis.asee.zuni.api.hourlies.HourMinimal;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface HourMinimalDao {

    @Insert(onConflict = REPLACE)
    void bulkInsert(List<HourMinimal> hourMinimalList);

    @Query("SELECT * FROM hourly WHERE city = :nombre")
    LiveData<List<HourMinimal>> getHourMinimalListByName(String nombre);

    @Query("DELETE FROM hourly WHERE city = :nombre")
    int deleteHourMinimalByName(String nombre);

    @Query("SELECT count(*) FROM hourly WHERE city = :nombre")
    int getNumberMinimalHourByName(String nombre);
}
