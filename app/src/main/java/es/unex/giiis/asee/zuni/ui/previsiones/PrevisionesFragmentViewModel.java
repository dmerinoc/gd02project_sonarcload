package es.unex.giiis.asee.zuni.ui.previsiones;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import es.unex.giiis.asee.zuni.api.daily.DatumMinimal;

public class PrevisionesFragmentViewModel extends ViewModel {


    private final DatumMinimalRepository mRepository;
    private final LiveData<List<DatumMinimal>> mDatumMinimals;
    private String mCiudad = "";
    private String mCountryCode = "";

    public PrevisionesFragmentViewModel(DatumMinimalRepository repository) {
        mRepository = repository;
        mDatumMinimals = mRepository.getCurrentDatumMinimals();
    }

    public void setName(String ciudad, String countryCode){
        if(ciudad!=null && countryCode != null){
            if(!ciudad.trim().equals("") && !countryCode.trim().equals("")){
                mCiudad = ciudad;
                mCountryCode = countryCode;
                mRepository.setName(ciudad,countryCode);
            }
        }
    }

    public void onRefresh() {
        mRepository.doFetchDatumMinimals(mCiudad,mCountryCode);
    }

    public LiveData<List<DatumMinimal>> getDatumMinimals() {
        return mDatumMinimals;
    }
}
