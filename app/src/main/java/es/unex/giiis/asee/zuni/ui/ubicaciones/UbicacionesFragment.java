package es.unex.giiis.asee.zuni.ui.ubicaciones;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import es.unex.giiis.asee.zuni.AddUbicacionActivity;
import es.unex.giiis.asee.zuni.FavUbicacionActivity;
import es.unex.giiis.asee.zuni.R;
import es.unex.giiis.asee.zuni.eventos.EventoRepository;
import es.unex.giiis.asee.zuni.eventos.db.EventoDatabase;
import es.unex.giiis.asee.zuni.openweather.AppExecutors;
import es.unex.giiis.asee.zuni.ubicaciones.Ubicacion;
import es.unex.giiis.asee.zuni.ubicaciones.UbicacionAdapter;
import es.unex.giiis.asee.zuni.ubicaciones.UbicacionAdapterListener;
import es.unex.giiis.asee.zuni.ubicaciones.UbicacionRepository;
import es.unex.giiis.asee.zuni.ubicaciones.db.UbicacionDatabase;
import es.unex.giiis.asee.zuni.ui.eventos.EventosFragmentViewModel;
import es.unex.giiis.asee.zuni.ui.eventos.EventosFragmentViewModelFactory;


public class UbicacionesFragment extends Fragment {

    // Codigo para peticion de añadir ubicacion
    private static final int ADD_UBICACION_REQUEST = 0;
    // Codigo para peticion de poner ubicacion predeterminada
    private static final int FAV_UBICACION_REQUEST = 1;

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private UbicacionAdapter mAdapter;
    private List<Ubicacion> ubicacionesList;

    //REPOSITORIO UBICACION
    //private UbicacionRepository mRepositoryUbi;

    // ViewModel
    private UbicacionesFragmentViewModel mViewModelUbi;
    // Factory
    private UbicacionesFragmentViewModelFactory mFactoryUbi;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_ubicaciones, container, false);

        Log.d("Zuni", "CARGANDO VISTA");

        /* FAB -> AÑADIR UBICACION ------------------------------------------------------------------*/
        FloatingActionButton fab = root.findViewById(R.id.addUbicaciones);
        fab.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), AddUbicacionActivity.class);
            startActivityForResult(intent, ADD_UBICACION_REQUEST);
        });

        /* -------------------------------------------------------------------------------------- */
        /* REPOSITORY --------------------------------------------------------------------------- */
        // Obtener instancia del repositorio
        //mRepositoryUbi = UbicacionRepository.getInstance(UbicacionDatabase.getInstance(getActivity()).getDao());

        /*mRepositoryUbi.getUbicaciones().observe(this, new Observer<List<Ubicacion>>() {
            @Override
            public void onChanged(List<Ubicacion> ubicaciones) {
                ubicacionesList=ubicaciones;
                AppExecutors.getInstance().mainThread().execute(() -> mAdapter.swap(ubicaciones));
                Log.d("Zuni", "NEW");
            }
        });
        */
        /* VIEW MODEL --------------------------------------------------------------------------- */

        // Instanciar el factory
        mFactoryUbi = new UbicacionesFragmentViewModelFactory(UbicacionRepository
                .getInstance(UbicacionDatabase.getInstance(getActivity()).getDao()));

        // Instanciar el ViewModel
        mViewModelUbi = new ViewModelProvider(this, mFactoryUbi)
                .get(UbicacionesFragmentViewModel.class);

        // Observar los eventos del ViewModel
        mViewModelUbi.getUbicaciones().observe(this, ubicaciones -> {
            ubicacionesList=ubicaciones;
            AppExecutors.getInstance().mainThread().execute(() -> mAdapter.swap(ubicaciones));

        });
        /* -------------------------------------------------------------------------------------- */
        /* -------------------------------------------------------------------------------------- */

        mRecyclerView = root.findViewById(R.id.ubicaciones_recycler_view);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        /* LISTENER DE CLICK EN LOS ITEMS DE LA LISTA ------------------------------------------- */
        mAdapter = new UbicacionAdapter(getActivity(), new UbicacionAdapterListener() {
            @Override
            public void deleteButtonOnClick(View v, int position) {

                Ubicacion ubicacion = ubicacionesList.get(position);

                /* BORRAR LA UBICACION DEL REPOSITORIO */
                //mRepositoryUbi.deleteUbicacion(ubicacion);
                mViewModelUbi.deleteUbicacion(ubicacion);
                AppExecutors.getInstance().mainThread().execute(() -> mAdapter.remove(ubicacion));
            }

            @Override
            public void favButtonOnClick(View v, int position) {
                Intent intent = new Intent(getContext(), FavUbicacionActivity.class);
                Ubicacion.packageIntent(intent, ubicacionesList.get(position));
                startActivityForResult(intent, FAV_UBICACION_REQUEST);
            }
        });

        mRecyclerView.setAdapter(mAdapter);
        UbicacionDatabase.getInstance(getActivity());


        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == ADD_UBICACION_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Ubicacion ubicacion = new Ubicacion(data);
                /* INSERTAR UBICACION EN EL REPOSITORIO */
                AppExecutors.getInstance().diskIO().execute(() -> {
                    //long id = mRepositoryUbi.insertUbicacion(ubicacion);
                    long id = mViewModelUbi.insertUbicacion(ubicacion);
                    ubicacion.setId(id);
                    AppExecutors.getInstance().mainThread().execute(() -> mAdapter.add(ubicacion));

                });
            }
        } else if (requestCode == FAV_UBICACION_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                Ubicacion ubicacionFav = new Ubicacion(data);

                /* ACTUALIZAR UBICACION EN LA BASE DE DATOS */
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        //mRepositoryUbi.updateUbicacion(ubicacionFav);
                        mViewModelUbi.updateUbicacion(ubicacionFav);
                    }
                });
            }
        }
    }



/* CICLO DE VIDA DEL ACTIVITY --------------------------------------------------------------- */
/*    @Override
    public void onResume() {
        super.onResume();
        loadItems();
    }
*/
/* CARGAR LOS ELEMENTOS --------------------------------------------------------------------- */
/*    private void loadItems(){
      AppExecutors.getInstance().diskIO().execute(new Runnable() {
          @Override
          public void run() {
              LiveData<List<Ubicacion>> liveDataUbi = mRepositoryUbi.getUbicaciones();
              AppExecutors.getInstance().mainThread().execute(() -> liveDataUbi.observe(getActivity(), new Observer<List<Ubicacion>>() {
                          @Override
                          public void onChanged(List<Ubicacion> ubicacions) {
                              List<Ubicacion> ubicacionesList = ubicacions;
                              mAdapter.load(ubicacionesList);
                          }
              }));
          }
      });
    }
*/
}