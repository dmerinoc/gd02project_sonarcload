package es.unex.giiis.asee.zuni.ubicaciones;


import android.util.Log;

import androidx.lifecycle.LiveData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import es.unex.giiis.asee.zuni.openweather.AppExecutors;
import es.unex.giiis.asee.zuni.ubicaciones.db.UbicacionDao;

public class UbicacionRepository {
    private static final String LOG_TAG = es.unex.giiis.asee.zuni.ubicaciones.UbicacionRepository.class.getSimpleName();

    // For Singleton instantiation
    private static es.unex.giiis.asee.zuni.ubicaciones.UbicacionRepository sInstance;
    private final UbicacionDao mUbicacionDao;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private final Map<String, Long> lastUpdateTimeMillisMap = new HashMap<>();
    private static final long MIN_TIME_FROM_LAST_FETCH_MILLIS = 30000;


    /* CONSTRUCTOR ------------------------------------------------------------------------------ */
    private UbicacionRepository(UbicacionDao ubicacionDao){
        mUbicacionDao = ubicacionDao;

        // LiveData que obtiene los eventos de la BD
        LiveData<List<Ubicacion>> dbData = ubicacionDao.getAll();

        // As long as the repository exists, observe the db LiveData.
        dbData.observeForever(newUbicacionesFromDB -> {
            mExecutors.diskIO().execute(() -> {

                if (dbData.getValue().size() == 0){
                    // Insertamos las nuevas ubicaciones en la BD
                    mUbicacionDao.bulkInsert(newUbicacionesFromDB);
                    Log.d(LOG_TAG, "New values inserted in Room");
                }
            });
        });
    }


    /* INSTANCIA SINGLETON ---------------------------------------------------------------------- */
    public synchronized static es.unex.giiis.asee.zuni.ubicaciones.UbicacionRepository getInstance(UbicacionDao dao){
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null){
            sInstance = new es.unex.giiis.asee.zuni.ubicaciones.UbicacionRepository(dao);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }


    /*
     * ----------------------------------------------------------------------------------------------
     * OPERACIONES DE LA BASE DE DATOS
     * ----------------------------------------------------------------------------------------------
     */


    /* GET UBICACION ------------------------------------------------------------------------------- */
    public Ubicacion getUbicacion(long id){
        Log.d(LOG_TAG, "OBTENIENDO UBICACION " + id);
        return mUbicacionDao.getUbicacion(id);
    }

    /* GET EVENTOS ------------------------------------------------------------------------------ */
    public LiveData<List<Ubicacion>> getUbicaciones(){
        Log.d(LOG_TAG, "OBTENIENDO UBICACIONES");
        return mUbicacionDao.getAll();
    }


    /* DELETE UBICACION ---------------------------------------------------------------------------- */
    public void deleteUbicacion(Ubicacion ubicacion){
        AppExecutors.getInstance().diskIO().execute(() -> mUbicacionDao.delete(ubicacion.getId()));
        Log.d(LOG_TAG, "BORRANDO UBICACION " + ubicacion.getUbicacion());
    }


    /* INSERT UBICACION ---------------------------------------------------------------------------- */
    public long insertUbicacion(Ubicacion ubicacion){
        Log.d(LOG_TAG, "INSERTANDO UBICACION " + ubicacion.getUbicacion());
        return mUbicacionDao.insert(ubicacion);
    }


    /* UPDATE UBICACION ---------------------------------------------------------------------------- */
    public void updateUbicacion(Ubicacion ubicacion){
        Log.d(LOG_TAG, "EDITANDO EVENTO " + ubicacion.getUbicacion());
        mUbicacionDao.update(ubicacion);
    }

}
